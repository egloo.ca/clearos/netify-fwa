<?php
// Netify Firewall Agent
// Copyright (C) 2015-2016 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

declare(ticks = 1);

if (getenv('NFA_BASEDIR') !== false)
    define('NFA_INCLUDE', getenv('NFA_BASEDIR'));
else
    define('NFA_INCLUDE', '.');

include_once(NFA_INCLUDE . '/include/nfa-version.php');

define('NFA_MARK_MASK', 0x800);
define('NFA_MARK_BITSHIFT', 11);

define('NFA_IPSET', '/usr/sbin/ipset');
define('NFA_IPTABLES', '/usr/sbin/iptables');
define('NFA_IP6TABLES', '/usr/sbin/ip6tables');
define('NFA_DEFAULT_TTL', 600);

define('NFA_FILE_CONF', '/etc/netify-fwa.conf');
define('NFA_FILE_PID', '/run/netify-fwa/netify-fwa.pid');
define('NFA_FILE_RELOAD_LOCK', '/run/netify-fwa/netify-fwa.reload');

define('NFA_FILE_PROTOS', '/var/lib/netify-fwa/nfa_protocols.dat');

define('NFA_NETIFY_NODE', '/var/run/netifyd/netifyd.sock');
define('NFA_NETIFY_SERVICE', 0);

define('NFA_SYSLOG_FACILITY', LOG_LOCAL0);

require_once('netify-proto.php');

// Global state
$nfa_state = array(
    'version' => NFA_VERSION,
    'config' => array(
        'file_conf' => NFA_FILE_CONF,
        'file_pid' => NFA_FILE_PID,
        'file_reload_lock' => NFA_FILE_RELOAD_LOCK,
        'node' => NFA_NETIFY_NODE,
        'service' => NFA_NETIFY_SERVICE,
        'syslog_facility' => NFA_SYSLOG_FACILITY,
        'rule_ttl' => NFA_DEFAULT_TTL,
        'disable_service_rules' => false,
        'disable_protocol_rules' => false,
        'rule_mark_mask' => NFA_MARK_MASK,
        'rule_mark_bitshift' => NFA_MARK_BITSHIFT
    ),
    'service_whitelist' => array(),
    'protocol_whitelist' => array(),
    'service_rules' => array(),
    'protocol_rules' => array(),
    'ipsets' => array(),
    'iptables' => array(),
    'debug' => false,
    'fwsync4' => false,
    'fwsync6' => false,
    'terminate' => false
);

function nfa_printf()
{
    global $nfa_state;

    if (func_num_args() < 2) return;

    $args = func_get_args();
    $level = array_shift($args);
    $format = array_shift($args);
    $message = vsprintf($format, $args);

    if ($nfa_state['debug'] == false)
        syslog($level, $message);
    else {
        switch ($level) {
        case LOG_EMERG:
            echo("[EMERG] $message");
            break;
        case LOG_ALERT:
            echo("[ALERT] $message");
            break;
        case LOG_CRIT:
            echo("[CRITICAL] $message");
            break;
        case LOG_ERR:
            echo("[ERROR] $message");
            break;
        case LOG_WARNING:
            echo("[WARNING] $message");
            break;
        case LOG_NOTICE:
            echo("[NOTICE] $message");
            break;
        case LOG_INFO:
            echo($message);
            break;
        case LOG_DEBUG:
        default:
            echo("[DEBUG] $message");
            break;
        }
    }
}

function nfa_usage($exit_code)
{
    global $nfa_state;

    $nfa_state['debug'] = true;

    nfa_printf(LOG_INFO, "Netify Firewall Agent v%s\n", NFA_VERSION);
    nfa_printf(LOG_INFO, "Copyright (C) 2015-2016 eGloo Incorporated <http://www.egloo.ca>\n");
    nfa_printf(LOG_INFO, " -c <filename>\n\tSet configuration filename (default: %s).\n",
        NFA_FILE_CONF);
    nfa_printf(LOG_INFO, " -n <node path/address>\n\tSet Netify UNIX/TCP node path/address (default: %s).\n",
        NFA_NETIFY_NODE);
    nfa_printf(LOG_INFO, " -s <service>\n\tNetify service paddress (TCP nodes only).\n");
    nfa_printf(LOG_INFO, " -m\n\tPrint configured base mark value and corresponding mask.\n");

    exit($exit_code);
}

function nfa_parse_rules($config_rules)
{
    global $nfa_state;
    global $nfa_protocols;
    $rules = array();

    if (! is_array($config_rules))
        throw new Exception('Unexpected rules datatype.');

    foreach ($config_rules as $id => $entry) {
        $parts = explode(',', $entry, 4);
        if (count($parts) != 4)
            throw new Exception("Malformed rule detected: $entry\n");

        $proto = $parts[2];
        if (!array_key_exists($proto, $nfa_protocols) &&
            !in_array($proto, $nfa_protocols)) {
            nfa_printf(LOG_ERR, "Invalid protocol: {$parts[2]}\n");
            continue;
        }

        if (array_key_exists($proto, $nfa_protocols))
            $proto_name = $nfa_protocols[$proto];
        else {
            $proto_name = $proto;
            $proto = array_search($proto_name, $nfa_protocols);
        }

        $rule = array(
            'table' => $parts[0],
            'chain' => $parts[1],
            'protocol' => intval($proto, 0),
            'protocol_name' => $proto_name,
            'enabled' => ($parts[3] == 0) ? false : true
        );

        $rules[$id] = $rule;
    }

    return $rules;
}

function nfa_load_config()
{
    global $nfa_state;

    $options = getopt('c:n:s:p:mdh');

    if ($options === false)
        nfa_usage(1);
    else if (array_key_exists('h', $options))
        nfa_usage(0);

    if (array_key_exists('d', $options))
        $nfa_state['debug'] = true;

    if (array_key_exists('c', $options))
        $nfa_state['config']['file_conf'] = $options['c'];

    $config = parse_ini_file($nfa_state['config']['file_conf'], true);
    if ($config === false) {
        throw new Exception('Error parsing configuration file: ' .
            $nfa_state['config']['file_conf']);
    }

    $nfa_state['config']['contents'] = $config;

    if (array_key_exists('nfa', $config)) {
        if (array_key_exists('file_pid', $config['nfa']))
            $nfa_state['config']['file_pid'] = $config['nfa']['file_pid'];

        if (array_key_exists('file_reload_lock', $config['nfa']))
            $nfa_state['config']['file_reload_lock'] = $config['nfa']['file_reload_lock'];

        if (array_key_exists('rule_ttl', $config['nfa']))
            $nfa_state['config']['rule_ttl'] = intval($config['nfa']['rule_ttl']);

        if (array_key_exists('syslog_facility', $config['nfa'])) {
            if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'daemon'))
                $nfa_state['config']['syslog_facility'] = LOG_DAEMON;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local0'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL0;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local1'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL1;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local2'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL2;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local3'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL3;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local4'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL4;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local5'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL5;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local6'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL6;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'local7'))
                $nfa_state['config']['syslog_facility'] = LOG_LOCAL7;
            else if (! strcasecmp(
                $config['nfa']['syslog_facility'], 'user'))
                $nfa_state['config']['syslog_facility'] = LOG_USER;
        }
        if (array_key_exists('disable_service_rules', $config['nfa'])) {
            if ($config['nfa']['disable_service_rules'] == 1 ||
                $config['nfa']['disable_service_rules'] == 'true' ||
                $config['nfa']['disable_service_rules'] == 'yes')
                $nfa_state['config']['disable_service_rules'] = true;
        }

        if (array_key_exists('disable_protocol_rules', $config['nfa'])) {
            if ($config['nfa']['disable_protocol_rules'] == 1 ||
                $config['nfa']['disable_protocol_rules'] == 'true' ||
                $config['nfa']['disable_protocol_rules'] == 'yes')
                $nfa_state['config']['disable_protocol_rules'] = true;
        }
    }

    if (array_key_exists('netify', $config)) {
        if (array_key_exists('node', $config['netify']))
            $nfa_state['config']['node'] = $config['netify']['node'];

        if (array_key_exists('service', $config['netify']))
            $nfa_state['config']['service'] = $config['netify']['service'];
    }

    if (array_key_exists('service_whitelist', $config))
        $nfa_state['service_whitelist'] = $config['service_whitelist'];

    if (array_key_exists('protocol_whitelist', $config))
        $nfa_state['protocol_whitelist'] = $config['protocol_whitelist'];

    if (array_key_exists('p', $options))
        $nfa_state['config']['file_pid'] = $options['p'];

    if (array_key_exists('m', $options)) {
        printf("NFA_MARK_MASK = 0x%08x\nNFA_MARK_BITSHIFT = %d",
            $nfa_state['config']['rule_mark_mask'],
            $nfa_state['config']['rule_mark_bitshift']
        );
        exit(0);
    }
}

function nfa_load_rules()
{
    global $nfa_state;

    $config = $nfa_state['config']['contents'];

    if (array_key_exists('service_rules', $config) &&
        array_key_exists('rule', $config['service_rules'])) {

        $nfa_state['service_rules'] = nfa_parse_rules(
            $config['service_rules']['rule']
        );
    }

    //var_dump($nfa_state['service_rules']);

    if (array_key_exists('protocol_rules', $config) &&
        array_key_exists('rule', $config['protocol_rules'])) {

        $nfa_state['protocol_rules'] = nfa_parse_rules(
            $config['protocol_rules']['rule']
        );
    }

    //var_dump($nfa_state['protocol_rules']);
}

function nfa_load_ipsets()
{
    global $nfa_state;

    $output = array();

    if (! nfa_exec(sprintf('%s -L -n', NFA_IPSET), $output)) {

        $nfa_state['ipsets'] = array();

        foreach ($output as $set) {
            nfa_printf(LOG_DEBUG, "set: $set\n");
            if (preg_match('/^NFA4_/', $set))
                $nfa_state['ipsets'][4][] = $set;
            else if (preg_match('/^NFA6_/', $set))
                $nfa_state['ipsets'][6][] = $set;
        }
    }
}

function nfa_save_pid($file_pid)
{
    if (file_put_contents($file_pid,
        sprintf("%d\n", posix_getpid()), LOCK_EX) === false)
        throw new Exception("Unable to save PID to: $file_pid\n");
}

function nfa_output_buffer($buffer)
{
    //nfa_printf(LOG_DEBUG, $buffer);
    nfa_printf(LOG_INFO, $buffer);
}

function nfa_daemonize()
{
    global $nfa_state;

    fclose(STDIN);
    fclose(STDOUT);
    fclose(STDERR);

    $STDIN = fopen('/dev/null', 'r');
    $STDOUT = fopen('/dev/null', 'w');
    $STDERR = fopen('/dev/null', 'w');

    openlog('netify-fwa', LOG_PID,
        $nfa_state['config']['syslog_facility']);

    ob_start('nfa_output_buffer');

    $pid = pcntl_fork();

    switch ($pid) {
    case -1:
        throw new Exception('Error during fork.');
    case 0:
        posix_setsid();

        break;
    default:
        exit(0);
    }

    switch ($pid) {
    case -1:
        throw new Exception('Error during second fork.');
    case 0:
        break;
    default:
        exit(0);
    }
}

function nfa_handle_signal($signal)
{
    global $nfa_state;

    switch ($signal) {
    case SIGHUP:
    case SIGINT:
    case SIGTERM:
        $nfa_state['terminate'] = true;
        nfa_printf(LOG_INFO, "Exiting...\n");
        break;
    case SIGUSR1:
        $nfa_state['fwsync4'] = true;
        break;
    case SIGUSR2:
        $nfa_state['fwsync6'] = true;
        break;
    default:
        nfa_printf(LOG_WARNING, "Unhandled signal: %d\n", $signal);
    }
}

function nfa_exec($command, &$output = null)
{
    nfa_printf(LOG_DEBUG, "%s\n", $command);

    $ph = popen("$command 2>&1", 'r');
    if (!is_resource($ph))
        throw new Exception('Error executing command: ' . $command);

    do {
        $buffer = trim(fgets($ph, 8192));
        if (strlen($buffer)) {
            if (is_array($output))
                $output[] = $buffer;
            else
                nfa_printf(LOG_ERR, "%s\n", $buffer);
        }
    } while (!feof($ph));

    return pclose($ph);
}

function nfa_socket_connect($node, $service)
{
    if ($node != 0) {
        $sd = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (!is_resource($sd))
            throw new Exception(socket_strerror(socket_last_error()));

        if (socket_connect($sd, $node, $service) === false)
            throw new Exception(socket_strerror(socket_last_error()));
    }
    else {
        $sd = socket_create(AF_UNIX, SOCK_STREAM, 0);
        if (!is_resource($sd))
            throw new Exception(socket_strerror(socket_last_error()));

        if (socket_connect($sd, $node) === false)
            throw new Exception(socket_strerror(socket_last_error()));
    }

    nfa_printf(LOG_DEBUG, "Connected to: %s%s%s\n", $node,
        ($service != 0) ? ':' : '',
        ($service != 0) ? $service : '');

    socket_set_nonblock($sd);

    return $sd;
}

function nfa_socket_read($sd, &$json, $length = 4096)
{
    $buffer = socket_read($sd, $length, PHP_NORMAL_READ);

    if ($buffer === false)
        throw new Exception(socket_strerror(socket_last_error()));
    else if (strlen($buffer) == 0)  return 0;

    $json = json_decode($buffer, true);

    if ($json === null) {
        nfa_printf(LOG_DEBUG, "Netify socket read %d bytes:\n\"%s\"\n",
            strlen($buffer), $buffer);
    }

    return strlen($buffer);
}

function nfa_validate_json($json)
{
    if (! array_key_exists('type', $json))
        throw new Exception('Malformed JSON.');

    switch ($json['type']) {
    case 'flow':
        nfa_validate_flow($json);
        break;

    case 'protocols':
        nfa_validate_protocols($json);
        break;

    case 'agent_hello':
        nfa_validate_agent_hello($json);
        break;

    case 'agent_status':
        nfa_validate_agent_status($json);
        break;

    default:
        throw new Exception('Unsupported JSON type.');
    }

    return $json['type'];
}

function nfa_validate_flow($flow)
{
    if (! array_key_exists('internal', $flow))
        throw new Exception('Malformed JSON.');

    if (! array_key_exists('flow', $flow))
        throw new Exception('Malformed JSON.');
}

function nfa_validate_protocols($protocols)
{
    if (! array_key_exists('protocols', $protocols))
        throw new Exception('Malformed JSON.');
}

function nfa_validate_agent_hello($hello)
{
    if (! array_key_exists('json_version', $hello))
        throw new Exception('Malformed JSON.');

    if ($hello['json_version'] > NFA_VERSION_JSON)
        throw new Exception('Unsupported Netify version.');

    if (! array_key_exists('build_version', $hello))
        throw new Exception('Malformed JSON.');

    nfa_printf(LOG_NOTICE, "%s\n", $hello['build_version']);
}

function nfa_validate_agent_status($status)
{
}

function nfa_process_flow($type, $flow, $whitelist, $rules)
{
    global $nfa_state;

    if (array_key_exists('local_ip', $whitelist)) {
        foreach ($whitelist['local_ip'] as $address) {
            if ($flow['local_ip'] == $address) {
//                nfa_printf(LOG_DEBUG,
//                    "Local address in %s whitelist: %s\n", $type, $address);
                return;
            }
        }
    }

    if (array_key_exists('remote_ip', $whitelist)) {
        foreach ($whitelist['remote_ip'] as $address) {
            if ($flow['other_type'] == 'remote' && $flow['other_ip'] == $address) {
//                nfa_printf(LOG_DEBUG,
//                    "Remote address in %s whitelist: %s\n", $type, $address);
                return;
            }
        }
    }

    if (array_key_exists('local_mac', $whitelist)) {
        foreach ($whitelist['local_mac'] as $address) {
            if ($flow['local_mac'] == $address) {
//                nfa_printf(LOG_DEBUG,
//                    "Local MAC in %s whitelist: %s\n", $type, $address);
                return;
            }
        }
    }

    if ($type == 'service') {
        if (array_key_exists('detected_application_name', $flow))
            $service = $flow['detected_application_name'];
        else
            $service = $flow['detected_service_name'];

        $protocol = preg_replace('/^\d+\./', '', $service);
    }
    else if ($type == 'protocol') $protocol = $flow['detected_protocol'];
    else throw new Exception("Invalid protocol type: $type");

    foreach ($rules as $rule) {
        if (($type == 'protocol' && $rule['protocol'] == $protocol) ||
            ($type == 'service' && $rule['protocol_name'] == $protocol) &&
            $rule['enabled'] === true)
            nfa_upsert_ipset($flow, $rule);
    }
}

function nfa_process_protocols($protocols)
{
    global $nfa_protocols;

    $nfa_protocols = array();

    foreach ($protocols['protocols'] as $protocol) {
        if (! array_key_exists('id', $protocol) ||
            ! array_key_exists('tag', $protocol)) {
            throw new Exception('Malformed JSON.');
        }

        if ($protocol['tag'] == 'Placeholder' ||
            $protocol['tag'] == 'Uninitialized') continue;

        $tag = preg_replace('/^\d+\./', '', $protocol['tag']);
        $nfa_protocols[$protocol['id']] = $tag;
    }

    nfa_printf(LOG_DEBUG, "Processed %d protocols/applications.\n",
        count($nfa_protocols));

    nfa_save_protocols(NFA_FILE_PROTOS);
}

function nfa_load_protocols($file_protos)
{
    if (!file_exists($file_protos)) return false;
    $buffer = file_get_contents($file_protos);
    if ($buffer === false)
        throw new Exception("Unable to load protos file: $file_protos\n");
    $protos = unserialize($buffer);
    if ($protos === false)
        throw new Exception("Unable to decode protos file: $file_protos\n");

    nfa_printf(LOG_DEBUG, "Loaded %d protocols/applications.\n",
        count($protos));

    return $protos;
}

function nfa_save_protocols($file_protos)
{
    global $nfa_protocols;

    if (file_put_contents($file_protos, serialize($nfa_protocols)) === false)
        throw new Exception("Unable to save protos file: $file_protos\n");

    nfa_printf(LOG_DEBUG, "Saved %d protocols/applications.\n",
        count($nfa_protocols));
}

function nfa_create_iptables($ip_version)
{
    global $nfa_state;

    $rules = array_merge(
        $nfa_state['service_rules'], $nfa_state['protocol_rules']);

    $table_chain_map = array();

    foreach ($rules as $rule) {
        $key = "{$rule['table']}:{$rule['chain']}";
        if (! array_key_exists($key, $table_chain_map))
            $table_chain_map[$key] = 0;
        $table_chain_map[$key] += ($rule['enabled']) ? 1 : 0;
    }

    foreach ($table_chain_map as $table_chain => $enabled) {
        list($table, $chain) = explode(':', $table_chain);
        foreach (array('INGRESS', 'EGRESS') as $direction) {
            if (nfa_exec(sprintf('%s -t %s -L %s_%s -n >/dev/null',
                ($ip_version == 4) ? NFA_IPTABLES : NFA_IP6TABLES,
                $table, $chain, $direction))) {
                nfa_exec(sprintf('%s -t %s -N %s_%s',
                    ($ip_version == 4) ? NFA_IPTABLES : NFA_IP6TABLES,
                    $table, $chain, $direction));
            }
            else {
                nfa_exec(sprintf('%s -t %s -F %s_%s >/dev/null',
                    ($ip_version == 4) ? NFA_IPTABLES : NFA_IP6TABLES,
                    $table, $chain, $direction));
            }
        }
    }

    foreach ($rules as $rule) {
        if (! $rule['enabled']) continue;
        foreach (array(
            'INGRESS' => 'src,src,dst', 'EGRESS' => 'dst,dst,src'
            ) as $direction => $param) {

            $ipset = strtoupper("nfa{$ip_version}_{$rule['protocol_name']}");

            if (! nfa_create_ipset($ipset, $ip_version,
                $nfa_state['config']['rule_ttl'])) return false;

            nfa_exec(sprintf('%s -t %s -A %s_%s -m set --match-set %s %s -j MARK --set-xmark 0x%x/0x%x',
                ($ip_version == 4) ? NFA_IPTABLES : NFA_IP6TABLES,
                $rule['table'], $rule['chain'], $direction, $ipset, $param,
                1 << $nfa_state['config']['rule_mark_bitshift'],
                $nfa_state['config']['rule_mark_mask'])
            );
        }
    }
}

function nfa_fwsync($ip_version)
{
    global $nfa_state;

    nfa_printf(LOG_DEBUG, "Synchronizing INET%d iptables hooks.\n", $ip_version);

    nfa_create_iptables($ip_version);
}

function nfa_create_ipset($name, $ip_version = 4, $rule_ttl = NFA_DEFAULT_TTL)
{
    global $nfa_state;

    if (array_key_exists($ip_version, $nfa_state['ipsets']) &&
        in_array($name, $nfa_state['ipsets'][$ip_version])) return true;

    $type = 'hash:ip,port,ip';
    $family = ($ip_version == 4) ? '' : $ip_version;

    if (nfa_exec(sprintf(
        '%s -q -t -f /dev/null -L %s', NFA_IPSET, $name))) {
        if (nfa_exec(sprintf('%s -N %s %s family inet%s timeout %d',
            NFA_IPSET, $name, $type, $family, $rule_ttl)))
            return false;
        else
            nfa_printf(LOG_DEBUG, "ipset: %s: created.\n", $name);
    }
    else nfa_printf(LOG_DEBUG, "ipset: %s: exists.\n", $name);

    $nfa_state['ipsets'][$ip_version][] = $name;

    return true;
}

function nfa_upsert_ipset($flow, $rule)
{
    global $nfa_state;

    $ipset = strtoupper("nfa{$flow['ip_version']}_{$rule['protocol_name']}");

    $rule = sprintf("%s -exist -A %s %s,%d:%d,%s",
        NFA_IPSET, $ipset,
        $flow['other_ip'], $flow['ip_protocol'], $flow['other_port'],
        $flow['local_ip']);

    nfa_printf(LOG_DEBUG, "$rule\n");

    return nfa_exec("$rule");
}

function nfa_main()
{
    $sd = null;
    global $nfa_state;
    global $nfa_protocols;

    pcntl_signal(SIGHUP,
        ($nfa_state['debug']) ? SIG_IGN : 'nfa_handle_signal');
    pcntl_signal(SIGINT, 'nfa_handle_signal');
    pcntl_signal(SIGTERM, 'nfa_handle_signal');
    pcntl_signal(SIGPIPE,
        ($nfa_state['debug']) ? SIG_IGN : 'nfa_handle_signal');
    pcntl_signal(SIGUSR1, 'nfa_handle_signal');
    pcntl_signal(SIGUSR2, 'nfa_handle_signal');

    while ($nfa_state['terminate'] === false) {

        if ($nfa_state['fwsync4'] === true) {
            nfa_fwsync(4);
            $nfa_state['fwsync4'] = false;
            unlink($nfa_state['config']['file_reload_lock']);
        }
        if ($nfa_state['fwsync6'] === true) {
            nfa_fwsync(6);
            $nfa_state['fwsync6'] = false;
            unlink($nfa_state['config']['file_reload_lock']);
        }

        if (! is_resource($sd)) {
            try {
                $sd = nfa_socket_connect(
                    $nfa_state['config']['node'],
                    $nfa_state['config']['service']);
            } catch (Exception $e) {
                nfa_printf(LOG_ERR,
                    "Error connecting to Netify node: %s\n",
                    $e->getMessage());
                sleep(1);
                continue;
            }
        }

        $fds_read = array($sd);
        $fds_write = NULL;
        $fds_except = NULL;

        $rc = @socket_select($fds_read, $fds_write, $fds_except, 1);

        if ($rc == 0) continue;
        if ($rc === false)
            throw new Exception(socket_strerror(socket_last_error()));

        if (in_array($sd, $fds_read)) {
            $json = null;
            $length = nfa_socket_read($sd, $json);

            if ($length == 0) {
                nfa_printf(LOG_NOTICE,
                    "Netify socket hung-up, reconnecting...\n");
                socket_close($sd);
                $sd = null;
                continue;
            }

            if ($json === null)
                throw new Exception('JSON decode error.');

            if (! array_key_exists('length', $json))
                throw new Exception('Malformed JSON, missing length.');

            //if ($nfa_state['debug']) var_dump($json);

            $payload_length = $json['length'];

            $json = null;
            $length = nfa_socket_read($sd, $json, $payload_length);

            if ($length == 0) {
                nfa_printf(LOG_NOTICE,
                    "Netify socket hung-up, reconnecting...\n");
                socket_close($sd);
                $sd = null;
                continue;
            }

            if ($length != $payload_length) {
                nfa_printf(LOG_DEBUG,
                    "Netify socket short read at %d bytes:\n", $length);
                throw new Exception('JSON short read.');
            }

            if ($json === null) {
                // XXX: Instead of bailing if we encounter malformed
                // JSON, emit a warning and skip it.
                //throw new Exception('JSON decode error.');
                nfa_printf(LOG_WARNING,
                    "JSON decode error, malformed.  Ignoring...\n");
                continue;
            }

            //if ($nfa_state['debug']) var_dump($json);

            switch (nfa_validate_json($json)) {
            case 'flow':
                //if ($nfa_state['debug']) var_dump($json);

                // Skip flows from external interfaces
                if (! $json['internal']) continue;

                //if ($nfa_state['debug']) var_dump($json);

                // Skip flow protocols that do not match:
                // XXX: This list should permit all protocols that support
                // IP ports.  If a protocol can be matched using ipset, then
                // it should be in this list.
                // - TCP: 6
                // - UDP: 17
                // - SCTP: 132
                // - UDPLite: 136
                if ($json['flow']['ip_protocol'] != 6 &&
                    $json['flow']['ip_protocol'] != 17 &&
                    $json['flow']['ip_protocol'] != 132 &&
                    $json['flow']['ip_protocol'] != 136) continue;

                if ($json['flow']['other_type'] != 'remote') continue;
                if ($json['flow']['detected_protocol_name'] == 'DNS') continue;

                if ($nfa_state['config']['disable_service_rules'] === false) {
                    nfa_process_flow('service', $json['flow'],
                        $nfa_state['service_whitelist'], $nfa_state['service_rules']);
                }
                if ($nfa_state['config']['disable_protocol_rules'] === false) {
                    nfa_process_flow('protocol', $json['flow'],
                        $nfa_state['protocol_whitelist'], $nfa_state['protocol_rules']);
                }
                break;

            case 'protocols':
                //if ($nfa_state['debug']) var_dump($json);
                nfa_process_protocols($json);
                if (($protocols = nfa_load_protocols(NFA_FILE_PROTOS)) !== false)
                    $nfa_protocols = $protocols;
                nfa_load_rules();
                break;
            }
        }
    }

    @unlink($nfa_state['config']['file_pid']);
    @unlink($nfa_state['config']['file_reload_lock']);
}

try {
    nfa_load_config();

    nfa_printf(LOG_INFO,
        "Netify Firewall Agent v%s/%s\n", NFA_VERSION, NFA_VERSION_JSON);

    if (($protocols = nfa_load_protocols(NFA_FILE_PROTOS)) !== false)
        $nfa_protocols = $protocols;

    nfa_load_rules();
    nfa_load_ipsets();
    foreach (array(4, 6) as $ip_version) nfa_fwsync($ip_version);

    if ($nfa_state['debug'] === false) {
        nfa_printf(LOG_INFO,
            "Netify Firewall Agent v%s starting...\n", NFA_VERSION);
        nfa_daemonize();
    }
    nfa_save_pid($nfa_state['config']['file_pid']);
    nfa_main();
} catch (Exception $e) {
    nfa_printf(LOG_ERR, "Exception: %s:%d: %s\n",
        basename($e->getFile()), $e->getLine(), $e->getMessage());
    exit(1);
}

exit(0);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
