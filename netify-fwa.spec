# Netify FWA Daemon

Name: netify-fwa
Version: 2.93
Release: 1%{dist}
Summary: Netify FWA
Vendor: eGloo Incorporated
License: GPLv3
Group: System/Daemons
Packager: eGloo Incorporated
Source: %{name}-%{version}.tar.gz
BuildArch: noarch
BuildRoot: /var/tmp/%{name}-%{version}
Requires: ipset
Requires: netifyd >= 2.93-1
Requires: app-firewall-core >= 1:2.7.5
Requires: webconfig-httpd
Requires: webconfig-php-cli
BuildRequires: systemd
BuildRequires: autoconf >= 2.69
BuildRequires: automake
%{?systemd_requires}

%description
Netify FWA Daemon

Report bugs to: https://gitlab.com/egloo.ca/clearos/netify-fwa/issues

# Prepare
%prep
%setup -q

./autogen.sh
%{configure}

# Install
%install
mkdir -p %{buildroot}/%{_sharedstatedir}/%{name}
mkdir -p %{buildroot}/%{_sharedstatedir}/%{name}/include

install -D -m 644 %{name}.php %{buildroot}/%{_datarootdir}/%{name}/%{name}.php
install -D -m 644 include/nfa-version.php %{buildroot}/%{_datarootdir}/%{name}/include/nfa-version.php
install -D -m 644 netify-proto.php %{buildroot}/%{_datarootdir}/%{name}/netify-proto.php
install -D -m 755 deploy/clearos/%{name} %{buildroot}/%{_sbindir}/%{name}
install -D -m 644 deploy/clearos/%{name}.service %{buildroot}/%{_unitdir}/%{name}.service
install -D -m 0644 deploy/clearos/%{name}.tmpf %{buildroot}/%{_tmpfilesdir}/%{name}.conf
install -D -m 0660 deploy/clearos/%{name}.conf %{buildroot}/%{_sysconfdir}/%{name}.conf
install -D -m 0755 deploy/clearos/exec-start-post.sh %{buildroot}/%{_libexecdir}/%{name}/exec-start-post.sh
install -D -m 0755 deploy/clearos/exec-stop-post.sh %{buildroot}/%{_libexecdir}/%{name}/exec-stop-post.sh
mkdir -p %{buildroot}/run
install -d -m 0755 %{buildroot}/run/%{name}

# Clean-up
%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# Post install
%post
%systemd_post %{name}.service
sed -i -e 's|/var/lib|/var/run|g' /etc/netify-fwa.conf || true

# Pre uninstall
%preun
%systemd_preun %{name}.service

# Post uninstall
%postun
%systemd_postun_with_restart %{name}.service

# Files
%files
%defattr(-,root,root)
%attr(755,root,root) %{_sbindir}/%{name}
%dir %attr(750,root,webconfig) %{_sharedstatedir}/%{name}
%dir %attr(750,root,webconfig) %{_sharedstatedir}/%{name}/include
%attr(755,root,root) %{_datarootdir}/%{name}/%{name}.php
%attr(755,root,root) %{_datarootdir}/%{name}/include/nfa-version.php
%attr(644,root,root) %{_datarootdir}/%{name}/netify-proto.php
%attr(755,root,root) %{_unitdir}
%dir /run/%{name}
%attr(644,root,root) %{_tmpfilesdir}/%{name}.conf
%attr(644,root,root) %{_sysconfdir}/%{name}.conf
%attr(644,root,root) %{_unitdir}/%{name}.service
%attr(755,root,root) %{_libexecdir}/%{name}/
%config(noreplace) %attr(664,root,root) %{_sysconfdir}/%{name}.conf

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
